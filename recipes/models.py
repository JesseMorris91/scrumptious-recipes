from django.db import models

class Recipe(models.Model):
    name = models.CharField(max_length = 125)
    author = models.CharField(max_length = 100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.name}"



class Measure(models.Model):
    measurement = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return f"{self.measurement}"

class FoodItem(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"

class Ingredient(models.Model):
    amount = models.FloatField()
    recipe = models.ForeignKey("Recipe", related_name="ingredients", on_delete=models.CASCADE)
    measure = models.ForeignKey("Measure", related_name="ingredients", on_delete=models.PROTECT)
    food = models.ForeignKey("FoodItem", related_name="ingredients", on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.amount}"

class Step(models.Model):
    recipe = models.ForeignKey("Recipe", related_name="steps", on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField()  #fields/ columns
    directions = models.CharField(max_length=300)

    def __str__(self):
        return f"{self.recipe}"
